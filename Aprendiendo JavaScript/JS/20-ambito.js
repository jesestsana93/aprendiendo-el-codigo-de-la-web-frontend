'use strict'

// El ambito de las variables

var texto = "Hola soy una varible global";

var numero = 10;

function HolaMundo(texto){
    var elemento = "Estoy adentro";
    
    console.log(texto);
    console.log(numero);
    console.log(elemento);
}

HolaMundo(texto);

// No se puede afuera
// console.log(elemento);







